import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LoginComponent } from '@app/admin/login/login.component';
import { PageNotFoundComponent } from '@app/core/page-not-found/page-not-found.component';

export  const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: '**', component: PageNotFoundComponent}
   
  ];
  
  export  const routing: ModuleWithProviders = RouterModule.forRoot(routes);