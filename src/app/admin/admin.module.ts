import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AdminRoutingModule } from '@app/admin/admin-routing.module';
import { SharedModule } from '@app/shared';
import { AdminComponent } from '@app/admin/admin.component';
//import { CoreModule } from '@app/core';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    //CoreModule,
    AdminRoutingModule
  ],
  exports: [
    AdminRoutingModule    
  ],
  declarations: [AdminComponent,RegisterComponent, LoginComponent]
})
export class AdminModule { }
