import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '@app/admin/login/login.component';
import { RegisterComponent } from '@app/admin/register/register.component';
import { PageNotFoundComponent } from '@app/core/page-not-found/page-not-found.component';

const loginRoutes: Routes = [
    {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full'
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    { path: '**', component: PageNotFoundComponent}     
  ];
  
  @NgModule({
    imports: [RouterModule.forRoot(loginRoutes)],
    exports: [RouterModule]
  })
  export class AdminRoutingModule { }