import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from '@app/core/page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';
import { AuthguardComponent } from './authguard/authguard.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    PageNotFoundComponent
  ],
  declarations: [PageNotFoundComponent, AuthguardComponent]
})
export class CoreModule { }
