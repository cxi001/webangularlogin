import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {  MatButtonModule, MatToolbarModule, MatMenuModule, MatCardModule, MatIconModule, MatAutocompleteModule, MatFormFieldModule, MatInputModule } from '@angular/material';

@NgModule({
    imports: [MatButtonModule, 
      MatToolbarModule,
      MatMenuModule,
      MatCardModule,
      MatIconModule,
      MatFormFieldModule,
      MatInputModule,
      MatAutocompleteModule],
    exports: [MatButtonModule, 
      MatToolbarModule,
      MatMenuModule,
      MatCardModule,
      MatIconModule,
      MatFormFieldModule ,
      MatInputModule,
      MatAutocompleteModule]
  })

export class MaterialAppModule { }